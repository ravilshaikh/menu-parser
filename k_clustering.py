from response import responsefile
import math 

responseArray=responsefile["Blocks"]
filter_line_blocks= list(filter(lambda d: d.get("BlockType")=="LINE", responseArray))
# print(len(filter_line_blocks))

for index in range(len(filter_line_blocks)):
    lineObject=filter_line_blocks[index]
    height=abs(lineObject["Geometry"]["Polygon"][1]["Y"]-lineObject["Geometry"]["Polygon"][2]["Y"])
    lineObject["height"]=height
    filter_line_blocks[index]=lineObject
# print(filter_line_blocks[1]["height"])
newlist = sorted(filter_line_blocks , key=lambda x: x.get("height"), reverse=False)
list_categorised=list(list())
index=0
single_category=list()

for i in range(len(newlist)):
    if i==0 :
        single_category.append(newlist[i])
        continue
    # print(i,newlist[i-1]["height"]*100,newlist[i]["height"]*100,(newlist[i]["height"]*100)-(newlist[i-1]["height"]*100))
    if (newlist[i]["height"]*100)-(newlist[i-1]["height"]*100)<=0.1:
        single_category.append(newlist[i])
    else:
        list_categorised.append(single_category)
        single_category=list()
        single_category.append(newlist[i])  
print((list_categorised[5]))


count_pair_list=list()
for i in range(len(list_categorised)):
    count_pair_list.append({"Object":list_categorised[i],"count":len(list_categorised[i])})

sorted_count_pair = sorted(count_pair_list, key=lambda x: x.get("count"), reverse=True)

k_distinct_center_object=list()
for i in range(len(sorted_count_pair[1]['Object'])):
    k_distinct_center_object.append([sorted_count_pair[1]['Object'][i]])

def mid_point_bounding_box(array_object,i):
    mid_x=0
    mid_y=0
    for j in range(len(array_object[i]["Geometry"]["Polygon"])):
        mid_x +=array_object[i]["Geometry"]["Polygon"][j]["X"]
        mid_y +=array_object[i]["Geometry"]["Polygon"][j]["Y"]
    x=mid_x/4
    y=mid_y/4  
    return [x,y]

k_distinct_center=list()
for i in range(len(k_distinct_center_object)):
    mid_x=0
    mid_y=0
    for j in range(len(k_distinct_center_object[i])):
        mid_x += k_distinct_center_object[i][0]["Geometry"]["Polygon"][j]["X"]
        mid_y += k_distinct_center_object[i][0]["Geometry"]["Polygon"][j]["Y"]
    x=mid_x/4
    y=mid_y/4 
    # print('[',x,',',1-y,'],')
    k_distinct_center.append({"X":x,"Y":y})

category_list=sorted_count_pair[0]['Object']
for  i in range(len(category_list)):
    [x,y]=mid_point_bounding_box(category_list,i)
    # print('[',x,',',1-y,'],')
    distance_array=list()
    for j in range(len(k_distinct_center)):
        c1=k_distinct_center[j]["X"]
        c2=k_distinct_center[j]["Y"]
        distance=math.sqrt((pow((x-c1),2)+pow((y-c2),2)))
        # print('distannce',distance,j)
        distance_array.append({"dist":distance,"index":j})
    sort_distance=sorted(distance_array, key=lambda x: x.get("dist"), reverse=False)
    for d in range(len(sort_distance)):
        j=sort_distance[d]["index"]
        if k_distinct_center[j]["Y"]<y:
            break;

    
    # print(j)
    k_distinct_center_object[j].append(category_list[i])
    total_points=len(k_distinct_center_object[j])
    new_x=(k_distinct_center[j]["X"]*(total_points-1)+x)/total_points
    new_y=(k_distinct_center[j]["Y"]*(total_points-1)+y)/total_points
    k_distinct_center[j]={"X":new_x,"Y":new_y}

# print(k_distinct_center_object)    



















