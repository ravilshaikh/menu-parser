from response import responsefile
import math 
import json
def get_closest_key(my_dict,given_key):
    res = my_dict.get(given_key) or my_dict[min(my_dict.keys(), key = lambda key: abs(key-given_key))]
    key_list = list(my_dict.keys())
    val_list = list(my_dict.values())
    position = val_list.index(res)
    return [key_list[position],position]

def get_line_blocks():
    responseArray=responsefile["Blocks"]
    line_blocks= list(filter(lambda d: d.get("BlockType")=="LINE", responseArray))
    sorted_line_block=sorted(line_blocks , key=lambda x: x.get("Geometry").get("BoundingBox").get("Top"),reverse=False)
    return sorted_line_block

def check_geometry_text(left_dict):
    for key, value in  left_dict.items():
        new_value_list=list()  
        for i in range(len(value)):
            new_value_list.append({"height":value[i]["Geometry"]["BoundingBox"]["Height"]*100,"index":i})    
        sorted_value_list=sorted(new_value_list, key=lambda x: x.get("height"),reverse=False)
        count_sort_value_list=list()
        for i in range(len(sorted_value_list)):
            if i==0:
                new_list=list()
                new_list.append(sorted_value_list[i])
                count_sort_value_list.append(new_list)
                continue
            if abs(sorted_value_list[i]["height"]-sorted_value_list[i-1]["height"])<=0.1:
                current_list=count_sort_value_list.pop(len(count_sort_value_list)-1)
                current_list.append(sorted_value_list[i])
                count_sort_value_list.append(current_list)
            else:
                new_list=list()
                new_list.append(sorted_value_list[i])
                count_sort_value_list.append(new_list)
        count_pair_list=list()
       
        for i in range(len(count_sort_value_list)):
            index_array=[]
            for j in range(len(count_sort_value_list[i])):
                index_array.append(count_sort_value_list[i][j]["index"])     
            count_pair_list.append({"index":index_array,"count":len(count_sort_value_list[i])})       
        sorted_count_pair = sorted(count_pair_list, key=lambda x: x.get("count"), reverse=False)   
        array_indexes=[]
        for i in range(len(sorted_count_pair)-1) :
           for j in range(len(sorted_count_pair[i]["index"])):
                array_indexes.append(sorted_count_pair[i]["index"][j])              
        for i in range(len(array_indexes)):
            value.pop(array_indexes[i])
        left_dict[key]=value 
    return left_dict       

def get_unique_left_clusters(sorted_line_block):
    left_value_dict={}
    for i in range(len(sorted_line_block)):
        left=sorted_line_block[i]["Geometry"]["BoundingBox"]["Left"]
        left_percentage=round(left*100)
        if i==0:
            new_list=list()
            new_list.append(sorted_line_block[i])
            left_value_dict[left_percentage]=new_list  
        else:  
            [closest_key,position]=get_closest_key(left_value_dict,left_percentage)
            if abs(closest_key-left_percentage)<=3:
                    val_list=left_value_dict.get(closest_key)
                    val_list.append(sorted_line_block[i])
                    left_value_dict[closest_key]=val_list
            else:
                new_list=list()
                new_list.append(sorted_line_block[i])
                left_value_dict[left_percentage]=new_list  

    data=check_geometry_text(left_value_dict)   
    with open('data.json', 'w') as outfile:
        json.dump(data, outfile)  


def main():
    sorted_line_block=get_line_blocks()
    get_unique_left_clusters(sorted_line_block)

if __name__=="__main__":
    main()        
    






