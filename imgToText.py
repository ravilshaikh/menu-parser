# import cv2
# import os,argparse
# import pytesseract
# from PIL import Image
  
# #We then Construct an Argument Parser
# ap=argparse.ArgumentParser()
# ap.add_argument("-i","--image",
#                 required=True,
#                 help="Path to the image folder")
# ap.add_argument("-p","--pre_processor",
#                 default="thresh", 
#                 help="the preprocessor usage")
# args=vars(ap.parse_args())
  
# #We then read the image with text
# images=cv2.imread(args["image"])
  
# #convert to grayscale image
# gray=cv2.cvtColor(images, cv2.COLOR_BGR2GRAY)
  
# #checking whether thresh or blur
# if args["pre_processor"]=="thresh":
#     cv2.threshold(gray, 0,255,cv2.THRESH_BINARY| cv2.THRESH_OTSU)[1]
# if args["pre_processor"]=="blur":
#     cv2.medianBlur(gray, 3)
      
# #memory usage with image i.e. adding image to memory
# filename = "{}.jpg".format(os.getpid())
# cv2.imwrite(filename, gray)
# text = pytesseract.image_to_string(Image.open(filename))
# os.remove(filename)
# print('TEXT',text)
  
# # show the output images
# cv2.imshow("Image Input", images)
# cv2.imshow("Output In Grayscale", gray)
# cv2.waitKey(0)
import cv2
import numpy as np
import pytesseract
from PIL import Image

# Path of working folder on Disk
src_path = "/home/ravil/Projects/PythonLearning/MenuToCSV/"

def get_string(img_path):
    # Read image with opencv
    img = cv2.imread(img_path)

    # Convert to gray
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Apply dilation and erosion to remove some noise
    kernel = np.ones((1, 1), np.uint8)
    img = cv2.dilate(img, kernel, iterations=1)
    img = cv2.erode(img, kernel, iterations=1)

    # Write image after removed noise
    cv2.imwrite(src_path + "removed_noise.png", img)

    #  Apply threshold to get image with only black and white
    #img = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 31, 2)

    # Write the image after apply opencv to do some ...
    cv2.imwrite(src_path + "thres.png", img)

    # Recognize text with tesseract for python
    result = pytesseract.image_to_string(Image.open(src_path + "thres.png"))

    # Remove template file
    #os.remove(temp)

    return result


print ('--- Start recognize text from image ---')
print (get_string('/home/ravil/Projects/PythonLearning/MenuToCSV/menu.png'))

print ("------ Done -------")